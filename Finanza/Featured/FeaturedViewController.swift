//
//  FeaturedViewController.swift
//  Finanza
//
//  Created by Willian Calazans on 04/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import UIKit

class FeaturedViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var value = [108.90, 23.10, 14.90]
    var establishment = ["Paypal", "Eventbrite", "Itunes.com/bill"]
    var product = ["Ingresso Salão do Automovel", "Ingresso Maze Fest", "iCloud 50GB"]
    var paymentMethod = ["Cash", "Credit Card", "Credit Card"]
    
    
    var currency = Currency()
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch(indexPath.item){
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "headerFeatured", for: indexPath) as! FeaturedHeaderCollectionViewCell
            
            return cell
            
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "newTransaction", for: indexPath) as! NewTransactionCollectionViewCell
            
            let expensesTouch = UITapGestureRecognizer(target: self, action: #selector(self.xpensesTouch))
            
            cell.expensesButton.addGestureRecognizer(expensesTouch)
            
            return cell
            
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "featuredCell", for: indexPath) as! FeaturedCollectionViewCell
            
            
            cell.initContent(valueString: currency.currencyConvert(value: value[indexPath.item - 2]), establishmentString: establishment[indexPath.item - 2], productString: product[indexPath.item - 2])
            
            return cell
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//        let viewController = storyboard.instantiateViewController(withIdentifier: "xpensesVC") as! XpensesViewController
        
//        FeaturedViewController().present(viewController, animated: true)
//
//        switch(indexPath.item){
//            case 1:
//                    addXpenses()
//                break;
//            default:
//                break;
//        }
    }
    
    @IBOutlet var overviewView: UIView!
    
    @IBOutlet weak var featuredCollectionView: UICollectionView!
    
    let url = NSURL(string: "http://alzan.com.br/apps/untitled/finanza.php")
    
    var name = [String]()
    var image = [String]()
    var local = [String]()
    
    let featuredModel = FeaturedModel()
    let dbController = DBController()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dbController.createDB()
        
        featuredCollectionView.delegate = self
        featuredCollectionView.dataSource = self
        
//        let featuredModel = FeaturedModel()
        
        featuredCollectionView.register(UINib(nibName: "FeaturedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "featuredCell")
        
        featuredCollectionView.register(UINib(nibName: "FeaturedHeaderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "headerFeatured")
        
        featuredCollectionView.register(UINib(nibName: "NewTransactionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "newTransaction")
        
        
//        featuredCollectionView.addSubview(overviewView)

        
        URLSession.shared.dataTask(with: (url as URL?)!, completionHandler: {(data, response, error) -> Void in
            self.featuredModel.parseJSON(data: (data ?? nil)!)
            
            self.name = self.featuredModel.name
            self.image = self.featuredModel.image
            self.local = self.featuredModel.createdBy;
            
            OperationQueue.main.addOperation({
                self.featuredCollectionView.reloadData()
            })
        // Do any additional setup after loading the view.
        
    }).resume()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return product.count + 2
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch(indexPath.item){
        case 0:
           return CGSize(width: UIScreen.main.bounds.width, height: 220.0)
            
        case 1:
            return CGSize(width: UIScreen.main.bounds.width, height: 80.0)

        default:
            return CGSize(width: UIScreen.main.bounds.width, height: 120.0)
            
        }
    }
    
    @objc func xpensesTouch(){
//        let storyboard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "xpensesVC") as? XpensesViewController
//
//        self.present(storyboard!, animated: true)
        
        performSegue(withIdentifier: "xpensesSegue", sender: self)
    }
}
