//
//  FeaturedModel.swift
//  Finanza
//
//  Created by Willian Calazans on 04/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import Foundation

class FeaturedModel {
   
    var id = [String]()
    var name = [String]()
    var image = [String]()
    var description = [String]()
    var otherImage = [String]()
    var date = [String]()
    var createdBy = [String]()
    
    func parseJSON(data: Data){
        
        if let jsonObj = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary {
            
//            print(jsonObj!.value(forKey: "events")!)
            if let jsonArray = jsonObj!.value(forKey: "events") as? NSArray{
                for array in jsonArray{
                    if let jsonDict = array as? NSDictionary {

                    let id = jsonDict.value(forKey: "id")
                    let name = jsonDict.value(forKey: "name")
                    let image = jsonDict.value(forKey: "banner")
                    let description = jsonDict.value(forKey: "description")
                    let otherImage = jsonDict.value(forKey: "other_image")
                    let date = jsonDict.value(forKey: "date")
                    let createdBy = jsonDict.value(forKey: "created_by")

                    self.id.append(id as! String)
                    self.name.append(name as! String)
                    self.image.append(image as! String)
                    self.description.append(description as! String)
                    self.otherImage.append(otherImage as! String)
                    self.date.append(date as! String)
                    self.createdBy.append(createdBy as! String)
   
                }
            }
        }  else {
         
            }
        }
//        print(name)
    }
}
