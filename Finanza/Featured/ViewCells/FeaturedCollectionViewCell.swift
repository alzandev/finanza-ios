//
//  FeaturedCollectionViewCell.swift
//  Finanza
//
//  Created by Willian Calazans on 04/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import UIKit

class FeaturedCollectionViewCell: UICollectionViewCell {

  
  
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var mainImage: UIImageView!
    
    @IBOutlet weak var product: UILabel!
    @IBOutlet weak var establishment: UILabel!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var date: UILabel!
    
    
    
    func initContent(valueString: String, establishmentString: String, productString: String){
        
//        userProfileName.text = profileName
//        imageLocal.text = imageLocalString
        
        establishment.text = establishmentString
        product.text = productString
        value.text = valueString
//
        backView.layer.cornerRadius = 7.0
        backView.layer.shadowColor = UIColor.lightGray.cgColor
        backView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        backView.layer.shadowRadius = 6.0
        backView.layer.shadowOpacity = 0.35
        
        
//        mainImage.LoadImageFromURL(urlString: mainImageString)
        
    }

}
