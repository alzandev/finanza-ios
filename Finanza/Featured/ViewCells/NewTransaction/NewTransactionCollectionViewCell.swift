//
//  NewTransactionCollectionViewCell.swift
//  Finanza
//
//  Created by Willian Calazans on 15/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import UIKit

class NewTransactionCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var incomeButton: UIView!
    @IBOutlet weak var expensesButton: UIView!
    
    weak var controller: FeaturedViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let incomeTouch = UITapGestureRecognizer(target: self, action: #selector(self.incomeTouch))

        incomeButton.addGestureRecognizer(incomeTouch)

        let expensesTouch = UITapGestureRecognizer(target: self, action: #selector(self.xpensesTouch))
        
        expensesButton.addGestureRecognizer(expensesTouch)
        
        incomeButton.layer.cornerRadius = 7.0
        incomeButton.layer.shadowColor = UIColor.lightGray.cgColor
        incomeButton.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        incomeButton.layer.shadowRadius = 6.0
        incomeButton.layer.shadowOpacity = 0.35
        
        expensesButton.layer.cornerRadius = 7.0
        expensesButton.layer.shadowColor = UIColor.lightGray.cgColor
        expensesButton.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        expensesButton.layer.shadowRadius = 6.0
        expensesButton.layer.shadowOpacity = 0.35
        
    }

    
    @objc func xpensesTouch(){
        let vc = inputViewController as? FeaturedViewController
            let storyboard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "xpensesVC") as? XpensesViewController
            
        vc!.present(storyboard!, animated: true)
        
    }

    @objc func incomeTouch(){
    }

}
