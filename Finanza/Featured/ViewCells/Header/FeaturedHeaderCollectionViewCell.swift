//
//  FeaturedHeaderCollectionViewCell.swift
//  Finanza
//
//  Created by Willian Calazans on 14/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import UIKit

class FeaturedHeaderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var currentValue: UILabel!
    @IBOutlet weak var lastTransaction: UILabel!
    
    var value = [108.90, 3.53, 18.00, 10.00, 10.62, 23.10, 14.90, 8.50, 11.92, 4.00, 10.04, 4.09, 12.04, 5.33, 7.50, 18.00, 10.00, 8.50, 9.99, 20.80, 10.48, 14.14, 10.45, 11.27, 3.96, 5.38, 7.71, 15.00, 11.15, 10.00, 18.00, 5.42, 6.50, 10.00, 10.98, 10.00, 170.00, 70.00, 229.63]
    
    let currency = Currency()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        var total: Double = 0.0
        
        for i in 0..<value.count{
            total = total + value[i]
        }
        
        currentValue.text = currency.currencyConvert(value: total)
        
    
        bgView.layer.cornerRadius = 7.0
        bgView.layer.shadowColor = UIColor.lightGray.cgColor
        bgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        bgView.layer.shadowRadius = 6.0
        bgView.layer.shadowOpacity = 0.5

    }

}
