//
//  DBController.swift
//  Finanza
//
//  Created by Willian Calazans on 17/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import UIKit
import SQLite3

class DBController {
    
    func createDB(){
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("FinanzaDB.sqlite")
        
        var db: OpaquePointer?
        
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK{
            print("OK DB \(fileURL)")
            createTable()
        }
    }
    
    func openDB() -> OpaquePointer? {
        
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("FinanzaDB.sqlite")

        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path, &db) == SQLITE_OK{
            print("Opened")
            return db
        } else {
            print("No Opened")
            return db
        }
    }
    
    func createTable(){
        
      let db = openDB()

      let createFinanza = "CREATE TABLE IF NOT EXISTS `FINANZA` (`id` int(5) NOT NULL,`establishment` varchar(500) NOT NULL, `product` varchar(500) NOT NULL,`value` double(9,2) NOT NULL,`category_id` int(5) NOT NULL,`timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`payment_id` int(5) NOT NULL,`type_id` int(5) NOT NULL)"
        
       let createCategory =  "CREATE TABLE IF NOT EXISTS `FINANZA_CATEGORY` (`id` int(5) NOT NULL PRIMARY KEY,`category` varchar(500) NOT NULL)"
        
       let createPayment =   "CREATE TABLE IF NOT EXISTS `FINANZA_PAYMENT` (`id` int(5) NOT NULL PRIMARY KEY, `payment` varchar(500) NOT NULL)"
        
       let createType =  "CREATE TABLE IF NOT EXISTS `FINANZA_TYPE` (`id` int(5) NOT NULL PRIMARY KEY,`type` varchar(500) NOT NULL)"
        
        if sqlite3_exec(db, createFinanza, nil, nil, nil) != SQLITE_OK{
            let errMSG = String(cString: sqlite3_errmsg(db))
            print("ERROR \(errMSG)")
        } else {
            print("OK MAIN")
        }
        
        if sqlite3_exec(db, createCategory, nil, nil, nil) != SQLITE_OK{
            let errMSG = String(cString: sqlite3_errmsg(db))
            print("ERROR \(errMSG)")
        } else {
            insertDefaultCategory()
            print("OK CATEGORY")
        }
        
        if sqlite3_exec(db, createPayment, nil, nil, nil) != SQLITE_OK{
            let errMSG = String(cString: sqlite3_errmsg(db))
            print("ERROR \(errMSG)")
        } else {
            print("OK PAYMENT")
            insertDefaultPayment()
        }
        
        if sqlite3_exec(db, createType, nil, nil, nil) != SQLITE_OK{
            let errMSG = String(cString: sqlite3_errmsg(db))
            print("ERROR \(errMSG)")
        } else {
            print("OK TYPE")
        }
    }
    
    func insertDefaultPayment(){
        
        var stmt: OpaquePointer?
        let db = openDB()
        
//        let paymentMethod = ["Cash", "Debit Card", "Credit Card", "Transfer", "Voucher", "Mobile Payment", "CryptoCurrency", "Others"]
//
//        let paymentId = [1, 2, 3, 4, 5, 6, 7, 8]
        
        let insertQuery = "INSERT INTO FINANZA_PAYMENT (id, payment) VALUES('1', 'Cash'),('2', 'Debit Card'),('3', 'Credit Card'),('4', 'Transfer'),('5', 'Voucher'),('6', 'Mobile Payment'), ('7', 'CryptoCurrency'),('8', 'Others');"
        
        if sqlite3_prepare(db, insertQuery, -1, &stmt, nil) == SQLITE_OK{
            
            if sqlite3_step(stmt) == SQLITE_DONE {
                print("successfully")
            }
            print("INSERTED")
        } else {
            let errMsg = String(cString: sqlite3_errmsg(db))
            print("ERROR ON INSERT\(errMsg)")
        }
    }
    
    func insertDefaultCategory(){
        
        var stmt: OpaquePointer?
        let db = openDB()
        
        let insertQuery = "INSERT INTO FINANZA_CATEGORY (id, category) VALUES('1', 'Food'), ('2', 'Health'), ('3', 'Education'), ('4', 'Transport'), ('5', 'Bills'), ('6', 'Bank Transfer'), ('7', 'Ecommerce'), ('8', 'Home'), ('9', 'Wear'), ('10', 'Sport'), ('11', 'Travel'), ('12', 'Others');"
        
//         let categoryMessage = ["Food", "Health", "Education", "Transport", "Bills", "Bank Transfer", "Ecommerce", "Home", "Wear", "Sport", "Travel", "Others"]
//         let categoryId = [1, 2, 3, 4, 5, 6, 7, 8, 9 ,10, 11, 12]
        
        if sqlite3_prepare(db, insertQuery, -1, &stmt, nil) == SQLITE_OK{
            
            if sqlite3_step(stmt) == SQLITE_DONE {
                print("successfully")
            }
            print("INSERTED")
        } else {
            let errMsg = String(cString: sqlite3_errmsg(db))
            print("ERROR ON INSERT\(errMsg)")
        }
    }
    
    func selectDefault(){
        let db = openDB()
        var stmt : OpaquePointer?
        
        let selectQuery = "SELECT * FROM FINANZA_PAYMENT"
        
        if sqlite3_prepare_v2(db, selectQuery, -1, &stmt, nil) != SQLITE_OK {
            let errMsg = String(cString: sqlite3_errmsg(db))
            print("SELECT PREPARE \(errMsg)")
        } else {
            print("ENTER HERE ")
        }
    
        while (sqlite3_step(stmt) == SQLITE_ROW){
            print("ENTER HERE TOO")
            let id = sqlite3_column_int(stmt, 0)
            let payment = String(cString: sqlite3_column_text(stmt, 1))

            print("XXX COD \(id)")
            print("XXX COD \(payment)")
        }
        
        if sqlite3_finalize(stmt) != SQLITE_OK {
            print("ERROR --> \(String(describing: sqlite3_errmsg(db)))")
        }
    
    }

}
