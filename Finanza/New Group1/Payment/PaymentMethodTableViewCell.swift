//
//  PaymentMethodTableViewCell.swift
//  Finanza
//
//  Created by Willian Calazans on 20/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import UIKit

class PaymentMethodTableViewCell: UITableViewCell {

    @IBOutlet weak var payment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setPayment(paymentMethod: String){
        payment.text = paymentMethod
    }
    
}
