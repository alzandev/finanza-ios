//
//  PaymentMethodViewController.swift
//  Finanza
//
//  Created by Willian Calazans on 20/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import UIKit

class PaymentMethodViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var paymentTableView: UITableView!
    
    let paymentMethod = ["Cash", "Debit Card", "Credit Card", "Transfer", "Voucher", "Mobile Payment", "CryptoCurrency", "Others"]
    
      let paymentImage = ["cash", "card", "card", "transfer", "voucher", "mobile", "cryptocurrency", "others"]
    
    let paymentId = [1, 2, 3, 4, 5, 6, 7, 8]
    
    @IBAction func closePayment(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentMethod.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        

        performSegue(withIdentifier: "returnToXpenses", sender: indexPath.item)

//        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "xpensesVC") as! XpensesViewController
//
////        vc.setPaymentMethod(method: paymentMethod[indexPath.row])
//
//        self.navigationController!.present(vc, animated: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let navController = segue.destination as? UINavigationController
        let xpensesVC = navController?.viewControllers.first as? XpensesViewController
        
        XpensesViewController.paymentMethod = paymentMethod[sender as! Int]
        XpensesViewController.paymentImageReceive = paymentImage[sender as! Int]
        XpensesViewController.paymentMethodId = paymentId[sender as! Int]
        
        print("SENDER RECEIVE ===> \(sender as! Int)")
//        if segue.identifier == "returnToXpenses" {
//            if sender != nil {
//                let xpensesVC = segue.destination as! XpensesViewController
//                xpensesVC.paymentMethod = sender as! String
//            }
//        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "paymentCell", for: indexPath) as! PaymentMethodTableViewCell
        
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        
        // Configure Cell
        cell.setPayment(paymentMethod: paymentMethod[indexPath.item])
        
        return cell
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        paymentTableView.delegate = self
        paymentTableView.dataSource = self
        
        //        let featuredModel = FeaturedModel()
        
        paymentTableView.register(UINib(nibName: "PaymentMethodTableViewCell", bundle: nil), forCellReuseIdentifier: "paymentCell")
        
        paymentTableView.tableFooterView = UIView()
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
