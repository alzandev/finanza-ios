//
//  XpensesCategoryUIViewController.swift
//  Finanza
//
//  Created by Willian Calazans on 25/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import UIKit

class XpensesCategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var categoryTableView: UITableView!
    
    let categoryMessage = ["Food", "Health", "Education", "Transport", "Bills", "Bank Transfer", "Ecommerce", "Home", "Wear", "Sport", "Travel", "Others"]
    
    
    let categoryImage = ["food", "health", "education", "transport", "bar-code", "bank_transfer", "ecommerce", "home", "wear", "sport", "travel", "others-category"]
    
    let categoryId = [1, 2, 3, 4, 5, 6, 7, 8, 9 ,10, 11, 12]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryMessage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "xpensesCategoryCell", for: indexPath) as! XpensesCategoryTableViewCell
        
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        
        // Configure Cell
        cell.setCategory(categoryLabel: categoryMessage[indexPath.item], categoryImagePath: categoryImage[indexPath.item])
        
        return cell
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        categoryTableView.delegate = self
        categoryTableView.dataSource = self
        
        //        let featuredModel = FeaturedModel()
        
        categoryTableView.register(UINib(nibName: "XpensesCategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "xpensesCategoryCell")
        
        categoryTableView.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "categoryXpenses", sender: indexPath.item)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let navController = segue.destination as? UINavigationController
        _ = navController?.viewControllers.first as? XpensesViewController
        
//        XpensesItems.init(category: categoryMessage[sender as! Int])
        XpensesViewController.category = categoryMessage[sender as! Int]
        XpensesViewController.categoryIconRefer = categoryImage[sender as! Int]
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
