//
//  XpensesCategoryTableViewCell.swift
//  Finanza
//
//  Created by Willian Calazans on 25/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import UIKit

class XpensesCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCategory(categoryLabel: String, categoryImagePath: String){
        
        category.text = categoryLabel
        categoryImage.image = UIImage(named: categoryImagePath)
        
    }

//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
}
