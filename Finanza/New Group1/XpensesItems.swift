//
//  XpensesItems.swift
//  Finanza
//
//  Created by Willian Calazans on 30/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import Foundation

class XpensesItems {
    var category: String = "Category"
    var paymentMethod: String = "Payment Method"
    
    init(category: String){
        self.category = category
    }
    
    init(paymentMethod: String){
        self.paymentMethod = paymentMethod
    }
}

