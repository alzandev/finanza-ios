//
//  XpensesViewController.swift
//  Finanza
//
//  Created by Willian Calazans on 17/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import UIKit

class XpensesViewController: UIViewController, UITextFieldDelegate {
    
    static var paymentMethod: String = "Payment Method"
    static var paymentImageReceive: String = "cash"
    static var paymentMethodId: Int = 0
    
    static var category: String = "Category"
    static var categoryIconRefer: String = "others-category"
    static var categoryID: Int = 0


    static var currencyStr: String = ""
    static var descriptionStr: String = ""
    static var notesStr: String = ""

    
    @IBOutlet weak var currencyField: UITextField!
    @IBOutlet weak var descriptionField: UITextField!
    @IBOutlet weak var paymentField: UILabel!
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet weak var paymentImage: UIImageView!
    
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryIcon: UIImageView!
    @IBOutlet weak var categoryView: UIView!
    
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var notes: UITextField!
    
    
    var items: XpensesItems? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        currencyField.delegate = self
        
        UINavigationBar.appearance().shadowImage = UIImage()
        
        setNeedsStatusBarAppearanceUpdate()
      
//        currencyField.keyboardType = UIKeyboardType.numberPad
        currencyField.delegate = self
        descriptionField.delegate = self
        notes.delegate = self
        
        definesPresentationContext = true
        
        currencyField.addDoneButtonToKeyboard(myAction: #selector(self.currencyField.resignFirstResponder))
        
        //Set currency prefix, if not have
        
//        if(!currencyField.text!.hasPrefix("R$")){
//            self.currencyField.text = "R$" + currencyField.text!
//        }
        currencyField.addTarget(self, action: #selector(myTextFieldDidChange), for: .editingChanged)

//
        //Segue for payment touch
        let paymentViewTouch = UITapGestureRecognizer(target: self, action: #selector(self.paymentTouch))
        
        //Segue for category touch
        let categoryViewTouch = UITapGestureRecognizer(target: self, action: #selector(self.categoryTouch))

        paymentView.addGestureRecognizer(paymentViewTouch)
        categoryView.addGestureRecognizer(categoryViewTouch)
        
       
        paymentField.text = XpensesViewController.paymentMethod
        paymentImage.image = UIImage(named: XpensesViewController.paymentImageReceive)
        
        categoryLabel.text = XpensesViewController.category
        categoryIcon.image = UIImage(named: XpensesViewController.categoryIconRefer)

        descriptionField.text = XpensesViewController.descriptionStr
        currencyField.text = XpensesViewController.currencyStr

        // Do any additional setup after loading the view.
    }
    
    @objc func myTextFieldDidChange(_ textField: UITextField) {
        
        if let amountString = textField.text?.currencyInputFormatting() {
            textField.text = amountString
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
         UINavigationBar.appearance().tintColor = UIColor.black
        
        XpensesViewController.descriptionStr = descriptionField.text!
        XpensesViewController.currencyStr = currencyField.text!

//        print(XpensesViewController.descriptionStr)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        self.view.endEditing(true)
        currencyField.resignFirstResponder()
        return true
    }
    
    
    @objc func paymentTouch(){
        performSegue(withIdentifier: "paymentSegue", sender: self)
    }
    
    @objc func categoryTouch(){
        performSegue(withIdentifier: "ToCategory", sender: self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }
    
    func setPaymentMethod(method: String){
        print(" ===> \(method)")
        paymentField.text = XpensesViewController.paymentMethod
    }
    
    func doneOnNumericKeypad(){
     
    }
}

extension UITextField{
    
    func addDoneButtonToKeyboard(myAction:Selector?){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 300, height: 40))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: myAction)
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
}

extension String {
    
    // formatting text for currency textField
    func currencyInputFormatting() -> String {
        
        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        formatter.currencySymbol = "R$"
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        
        var amountWithPrefix = self
        
        // remove from String: "$", ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count), withTemplate: "")
        
        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: (double / 100))
        
        // if first number is 0 or all numbers were deleted
        guard number != 0 as NSNumber else {
            return ""
        }
        
        return formatter.string(from: number)!
    }
}

