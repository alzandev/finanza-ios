//
//  OverviewViewController.swift
//  Finanza
//
//  Created by Willian Calazans on 06/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import UIKit

class OverviewViewController: UIViewController {

    @IBOutlet weak var overviewCard: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        overviewCard.layer.cornerRadius = 10.0
        overviewCard.layer.shadowColor = UIColor.gray.cgColor
        overviewCard.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        overviewCard.layer.shadowRadius = 12.0
        overviewCard.layer.shadowOpacity = 0.7
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
