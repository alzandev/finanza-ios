//
//  Currency.swift
//  Finanza
//
//  Created by Willian Calazans on 16/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import UIKit

class Currency: NSObject {
    
    func currencyConvert(value: Double) -> String{
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.currencyAccounting
        formatter.locale = Locale.current
        formatter.currencyCode = "BRL"
      
        return formatter.string(for: value) ?? "0.00"
    
    }
}
