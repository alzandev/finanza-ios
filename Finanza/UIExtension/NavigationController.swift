//
//  File.swift
//  Finanza
//
//  Created by Willian Calazans on 25/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    open override var childForStatusBarStyle: UIViewController?{
        return self.topViewController
    }

    open override var childForStatusBarHidden: UIViewController?{
        return self.topViewController
    }

}
