//
//  loadImageFromURL.swift
//  Finanza
//
//  Created by Willian Calazans on 05/11/18.
//  Copyright © 2018 Willian Calazans. All rights reserved.
//

import UIKit
import Foundation

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    
    func LoadImageFromURL(urlString: String) {
        
        self.image = nil
        
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = cachedImage
            return
            
        }
        
        let url = URL(string: urlString)
        
        if(url != nil){
            
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async  {
                if let downloadedImage = UIImage(data: data!) {
                    imageCache.setObject(downloadedImage, forKey: urlString as AnyObject)
                    
                    self.image = downloadedImage
                    
                    self.contentMode = .scaleAspectFill
                    self.translatesAutoresizingMaskIntoConstraints = false
                    
                }
                
            }
            
        }).resume()
        
    }
    }
    
}
